// The project function defines how your document looks.
// It takes your content and some metadata and formats it.
// Go ahead and customize it to your liking!
#let project(title: "", authors: (), date: none, discipline: "Discipline", semester: "Semester", body) = {
  // Set the document's basic properties.
  set document(author: authors, title: title)
  set page(numbering: "1", number-align: center)
  set text(font: "Source Sans Pro", lang: "en")
  set heading(numbering: "1.1")

  // Set run-in subheadings, starting at level 3.
  show heading: it => {
    if it.level > 2 {
      parbreak()
      text(11pt, style: "italic", weight: "regular", it.body + ".")
    } else {
      it
    }
  }

  // numbering the math equation
  set math.equation(numbering: "(1)")

  // set different ref rules for equation
  show ref: it => {
    let eq = math.equation
    let el = it.element
    if el != none and el.func() == eq {
      // Override equation references.
      numbering(
        el.numbering,
        ..counter(eq).at(el.location())
      )
    } else {
      // Other references as usual.
      it
    }
  }

  // Display inline code in a small box
  // that retains the correct baseline.
  show raw.where(block: false): box.with(
    fill: luma(240),
    inset: (x: 3pt, y: 0pt),
    outset: (y: 3pt),
    radius: 2pt,
  )

  // Display block code in a larger block
  // with more padding.
  show raw.where(block: true): block.with(
    fill: luma(240),
    inset: 10pt,
    radius: 4pt,
  )



  rect(height: auto, width: 100%, stroke: 1pt)[#stack(
  dir: ttb, spacing: 1em,
  stack(dir: ltr,spacing: 1fr, rect(width: auto, stroke: none)[#text(weight: "bold", discipline)], rect(width:auto, stroke: none)[#text(weight: "bold", semester)]),
  stack(dir: ltr, 1fr, rect(width: auto, stroke: none)[#text(size: 1.75em, weight: "extrabold", title)], 1fr),
  stack(dir: ltr, rect(width: auto, stroke: none)[#text(style: "italic", [Author: #authors.join(", ", last: " and ")])], 1fr, rect(width: auto, height: auto, stroke: none)[#text(style: "italic", [#date.display("[month repr:long] [day padding:space], [year]")] )]),
  )]

  // // Title row.
  // align(center)[
  //   #block(text(weight: 700, 1.75em, title))
  //   #v(1em, weak: true)
  //   #date
  // ]

  // // Author information.
  // pad(
  //   top: 0.5em,
  //   bottom: 0.5em,
  //   x: 2em,
  //   grid(
  //     columns: (1fr,) * calc.min(3, authors.len()),
  //     gutter: 1em,
  //     ..authors.map(author => align(center, strong(author))),
  //   ),
  // )

  // Main body.
  set par(justify: true)

  body
}

