#import "template.typ": *
#import "theorem.typ": *

// Take a look at the file `template.typ` in the file panel
// to customize this template and discover how it works.
#show: project.with(
  title: "Note Template",
  authors: (
    "victbr", "xioxio", "anonymous"
  ),
  date: datetime.today(),
  discipline: "miscellaneous", 
  semester: "Fall 2023", 
)

#show: thmrule


// We generated the example code below so you can see how
// your document will look. Go ahead and replace it with
// your own content!

= Introduction
#lorem(60)

#thmbox([aaaa]) <test_theorem>

#theorem([aaa])

#lemma(short: "aaa", [bbb]) <lemma2>
123
#let pat = pattern(
  size: (30pt, 30pt),
  relative: "parent",
  square(
    size: 30pt,
    fill: gradient
      .conic(..color.map.rainbow),
  )
)
123123
@test_theorem
123123

@lemma2 
#let fillcolor(item) = {
  text(fill: pat, item)
} 

#fillcolor(lorem(10))



#show: thmrule

#figure([123123], kind: "thmenv", supplement: "thm")


== In this paper
#lorem(20)

```typ inline test```

```c++
#include<iostream>

using namespace std;

int main(){
  return 0;
}
```

=== Contributions
#lorem(40)

= Related Work
#lorem(500)


#bibliography("works.bib", full: true, style: "institute-of-electrical-and-electronics-engineers")