// Show Theorem rule.
// reference https://github.com/sahasatvik/typst-theorems


// define my numbering function.
// #let numbering_math = (..nums) =>  (
//       (
//         locate(loc=>(if counter(heading).at(loc).len() >= 1 [#counter(heading).at(loc).at(0)] else [0])),
//         locate(loc=>(if counter(heading).at(loc).len() >= 2 [#counter(heading).at(loc).at(1)] else [0])),
//       ) + nums.pos().map(str)).join(".")

// #let numbering_func = numbering_math 
      
#let thmbox(prefix: "Theorem", short: none, content) = {
  return figure(
    [#if short != none [ (#short)*.* ] else [*.* ] #content],
    kind: "thmenv",
    // numbering: numbering_func,
    // numbering: "1.", 
    supplement: prefix,
    // caption: "",
  )
}

#let theorem = thmbox.with(prefix: "Theorem")
#let lemma(short: none, content) = thmbox(prefix: "Lemma", short: short, content)
#let definition = thmbox.with(prefix: "Definition")
#let exercise = thmbox.with(prefix: "Exercise")
#let definition = thmbox.with(prefix: "Definition")
#let proposition = thmbox.with(prefix: "Proposition")
#let corollary = thmbox.with(prefix: "Corollary")
#let axiom =thmbox.with(prefix: "Axiom")
#let example = thmbox.with(prefix: "Example")
#let remark = thmbox.with(prefix: "Remark")

#let thmrule(content) = {
  // set figure.caption(separator: "")
  show figure.where(kind: "thmenv") : it => [#text( weight: "bold", style: "italic", [#it.supplement #it.counter.display()])#it.body]
  content
}



//@todo
#let proof = content => {
  "proof:" 
  content 
  $qed$
}