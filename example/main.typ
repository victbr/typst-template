#import "template.typ": *
#import "theorem.typ": *

// Take a look at the file `template.typ` in the file panel
// to customize this template and discover how it works.
#show: project.with(
  title: "Chaining",
  authors: (
    "xioxio",
  ),
  date: datetime(
    year: 2023,
    month: 12,
    day: 14
  ),
  discipline: "High-dimension probability", 
  semester: "Fall 2023", 
)

#show: thmrule


// We generated the example code below so you can see how
// your document will look. Go ahead and replace it with
// your own content!

= Dudley's inequality

From Sudakov's minoration inequality we get a lower bound on the magnitude 

$ EE sup_(t in T) X_t $

of a Gaussain tandom process $(X_t)_(t in T)=angle.l g, t angle.r, g tilde.op N(0,1)$ internms of the metrtic entropy of T. By Dudley's ineuqality we can get a similar uppder bound and with more general processes with sub-gaussian increments.

#definition(short: "Sub-gaussian increments")[
  Consider a random process $(X_t)_(t in T)$ on a metric space $(T, d)$. We say that the process has sub-gaussian increments if there exists $K >= 0$ such that
  $ norm(X_t-X_s)_(psi_2) <= K d(t,s) " for all " t,s in T $ <8.1>
]<sub-gaussian_increments>

#example()[
  Let $(X_t)_(t in T)$ be a Gaussian process on an abstract set $T$. Define a metric on $T$ by
  $ d(t,s) colon.eq norm(X_t - X_s)_(L^2), t,s in T $
  Then $(X_t)_(t in T)$ is obviously a process with sub-gaussian increments, and $K$ is an absolute constant. 
]<example1>

#proof()[
  by property of sub-gaussian, we have $ norm(X)_(L_p) <= C norm(X)_(psi_2) sqrt(p) $

  $ d(t,s)^2 = EE norm(X_t-X_s)^2 = norm(t)_2^2 + norm(s)_2^2 - 2 angle.l t, s angle.r = norm(t-s)_2^2 $

  $ 
  norm(X_t-X_s)_(psi_2) &= norm(angle.l g"," t - s angle.r)_(psi_2) \
  & = norm(sum_i g_i (t_i-s_i))_(psi_2) \
  norm(X_t-X_s)^2 &= norm(sum_i g_i (t_i-s_i))_(psi_2)^2 \
  & <= sum_i norm(g_i (t_i-s_i))_(psi_2)^2  \
  &= sum_i (t_i-s_i)^2 norm(g_i)_(psi_2)^2 = norm(t-s)_2^2 C_2
  $
]

Dudley's inequality can give a bound on a general sub-gaussian random process $(X_t)_(t in T)$ in terms of the metric entropy $log cal(N)(T,d,epsilon)$ of T.

#theorem(short: "Dudley's integral inequality")[
  Let $(X_t)_(t in T)$ be a mean zero random process on a metric space $(T,d)$ wiht sub-gaussian increments as in @8.1. Then 
  $
    EE sup_(t in T) X_t <= C K integral_0^oo sqrt(log cal(N)(T,d,epsilon)) d epsilon
  $
]<Dudleys_inequality>

To prove @Dudleys_inequality, we can firts prove a discrete version. 

#theorem(short:"Discrete Dusley's inequality")[
  Let $(X_t)_(t in T)$ be a mean zero random process on a metric space $(T,d)$ wiht sub-gaussian increments as in @8.1. Then 
  $
    EE sup_(t in T) X_t <= C K sum_(k in ZZ) 2^(-k) sqrt(log cal(N)(T,d,2^(-k)))
  $ <8.2>
]<Dudleys_inequality_discrete>

The proof of @Dudleys_inequality_discrete will be based on the important technique of chaining, which can be useful in many other problems. Chaining is a multi-scale version of the $epsilon$   -net argument. 

*main idea of proving @Dudleys_inequality_discrete*
If we sampple a finity set $cal(N)$ of $T$ and let $pi(t)$ to represnet the closest point to $t$, we will get a great bound $norm(X_t-X_(pi(t)))_(psi_2) <= K epsilon$, where $cal(N)$ is a $epsilon$-net of $T$. It gives $ EE sup_(t in T) X_t <= EE sup_(t in T) X_(pi(t)) + EE sup_(t in T)(X_t - X_(pi(t))) $

#proof()[
  We can choose $epsilon_k = 2^(-k)$, and consider finity set $T_k = cal(N)(T, d, epsilon_k)$. There is some measurablity issue of infinite $T$, so we only consider every finite subset $T'$ of T. Then for each finity set $T$, there exists some $k$ such that $T_k = T$. So $ X_t - X_(t_0) = & (X_t - X_(pi_k (t)) ) + ( X_(pi_k (t) ) - X_(pi_(k-1)(t) ) ) + dots.c \
  & + (X_(pi_(kappa+1)(t)) - X_(pi_kappa(t)) ) + (X_(pi_kappa (t)) - X_(t_0) ) \
  =& sum (X_(pi_k(t)) - X_(pi_(k-1)(t)) ) $

  
  Here $kappa$ is the first item such that $|T_kappa| = 1$.each item is a sub-gaussian random variable, and 
  $ 
  norm(X_(pi_k (t)) - X_(pi_(k-1)(t)))_(psi_2) <= & K d(pi_k (t), pi_(k-1)(t)) &, "by sub-gaussian increments" \
  <= & K ( d(pi_k (t), t) + d(pi_(k-1)(t), t) )&, "by property of distance" \
  <= & K ( epsilon_k + epsilon_(k-1) )& \
  <= & 2 K epsilon_(k-1)
  $

  Note that $sup_(t in T)(X_t - X_0) <= sum sup (X_(pi_k (t)) - X_(pi_(k-1) (t)))$, and $sup (X_(pi_k (t)) - X_(pi_(k-1) (t)))$ is the max value of $|T_k||T_(k-1)|$ sub-gaussian random variable. The expection of maximum of $N$ sub-gaussian with $K = max norm(X_i)_(psi_2)$ is 
  $
  EE max_(i <= N) |X_i| <= C K sqrt(log N)
  $<maximum_of_subgaussian>
  Here we have 
  $
  EE sup_(t in T) (X_() - X_()) <= C K epsilon_(k-1) sqrt(log cal(N)(T, d, epsilon_(k-1)))
  $<8.11>
  
  
  
]

#remark(short: "Supremum of increments")[
  remove the condition of zero mean we have
  $
    EE sup_(t in T) abs(X_t - X_(t_0)) <= C K integral_0^oo sqrt(log cal(N)(T,d,epsilon)) d epsilon
  $

  like prove in @Dudleys_inequality_discrete, here we just keep the item $X_(pi_1)(t) - X_(t_0)$, and each item with $abs(dot)$. The @8.11 still holds according to @maximum_of_subgaussian. 

  Note: only bounded $T$ will have finite $cal(N)(T, d, epsilon)$. With large enough $epsilon$, $|cal(N)(T,d,epsilon)|=1$, and we can uniform randomly pick the point. Thus $EE (X_(t_0)-X_(pi_kappa (t))) = 0$. 

  Update: for large enough $epsilon$, we can actually pick $T_kappa = {X_(t_0)}$
]

#theorem(short: "Dudley's integral inequality: tail bound")[
  Let $(X_t)_(t in T)$ be a random process on a metric space $(T,d)$ with sub-gaussian increments as in @8.1. Then for every $u >= 0$, the event 
  $
  sup_(t,s in T) abs(X_t - X_s) <= C K [integral_0^oo sqrt(log cal(N)(T,d,epsilon)) d epsilon + u dot.c "diam"(T)]
  $
  holds with probability at least $1-2 exp(-u^2)$. 
]<Dudley_inequality_tailbound>

#exercise()[
  Prove @Dudley_inequality_tailbound. To this end, first obtain a high probability version of @8.11 : 
  $
  sup_(t in T) (X_(pi_k (t)) - X_(pi_(k-1) (t))) <= C epsilon_(k-1) [sqrt(log abs(T_k)) + z]
  $<eq16>
  with probability at least $1-2exp(-z^2)$. Finally, choose a good $z_k$
]

#proof()[
  Sub-gaussian increments still holds, assume that $K=1$(we can use $Y_t = 1/k X_t$, and in the result replace $Y_t$ by $1/k X_t$). 

  @eq16 can be seen as $|T_k||T_(k-1)|$ not independent sub-gaussian random variable with $norm(X_t-X_s)_(psi_2) <= d(t,s) <= 2 epsilon_(k-1)$

  From property of sub-gaussian random variable, we know that 
  $ 
  PP[abs(X_t-X_s) > t] < 2 exp(-t^2/(C K^2)), K = norm(X_t-X_s)_(psi_2)
  $

  Let $t = C epsilon_(k-1)[sqrt(abs(T_(k-1))) + z]$, left size is $2exp(-C|T_(k-1)|-z^2)$.
  
  *Note*: here use a techinque that $a^2+b^2 <= (a+b)^2 <= 2(a^2+b^2), a,b >= 0$

  Then by union bound, we have 
  $
  &PP[sup_(t in T) (X_(pi_k (t))-X_(pi_(k-1) (t))) >= C_1 epsilon_(k-1) [sqrt(log abs(T_(k-1))) + z]] \
  <=& 2 abs(T_(k-1))/exp(C_2 abs(T_(k-1))) exp(-C_2 z^2)
  $
  pick appropriate $C_2 > 1$, the right side is lower than $exp(-z^2)$.

  Then we get @eq16. 

  Next, use the same technique as @Dudleys_inequality_discrete, we have 
  $
  sup_(t in T)abs(X_t-X_s) <= C K sum_(k = kappa)^K 2^(-k) (sqrt(log cal(N)(T,d,epsilon))+ z)
  $<eq19>
  with probability at least $1 - 2(K-kappa)exp(-z^2)$

  If we choose $z_k$ as a decrease function, we have 
  $
  sup_(t in T)abs(X_t-X_s) <= C K [integral_0^oo sqrt(log cal(N)(T,d,epsilon)) d epsilon +  integral_0^oo z(epsilon) d epsilon]
  $
  with probability at least $1 -  2 integral_0^oo exp(-z(epsilon)^2) d epsilon$

  Note that for large enough $epsilon$, $|cal(N)(T,d,epsilon)|=1$, and we can pick $X_(pi_k (t)) = X_(pi_(k-1) (t))$, then the item can be remove. It holds for $epsilon> "diam" T$, so the upperside of intergral can be $"diam" T$ instead of $oo$. 

  From @eq19, we can set $z_k = u + sqrt(k-kappa)$, then 
  $
  sup_(t in T)abs(X_t-X_s) <= C K sum_(k = kappa)^K 2^(-k) (sqrt(log cal(N)(T,d,epsilon))+ u + sqrt(k-kappa))
  $
  with probability at least $1 - sum_(k=kappa)^("diam" T) 2exp(-(u^2 + k-kappa))$. Discard the term $2u sqrt(k-kappa)$ to make it easier to handle with and it becomes lower. And then it it larger than $1 - 2 exp(-u^2)$. 

  Next from discrete to continuous, we have
  $
  sup_(t in T)abs(X_t-X_s) <= & C K integral_0^oo sqrt(log cal(N)(T,d,epsilon)) d epsilon + 2^(-kappa) sum_(k=0)^oo (u+sqrt(k))/2^k \
  <= & C K [integral_0^oo sqrt(log cal(N)(T,d,epsilon))d epsilon + 2 u dot.c "diam"(T) + integral_0^"diam(T)"sqrt(k-kappa)d epsilon]
  $
  Here is a problem that $integral_0^"diam(T)" sqrt(-log epsilon-kappa) d epsilon$ turns to be infinity. We need to handle this in discrete part. 

  This term's contribution is $C K 2^(-k) sqrt(k-kappa)$, can be rewrite as $sum_(k=kappa)^oo 2^(-k) sqrt(k-kappa) <= sum_(k=kappa)^oo (k-kappa)/2^k = sum_(k=0)^oo 1/2^kappa k/2^k <= 2^(2-kappa) approx 4 "diam"(T)$.

  Then we get the right side is $C K (u+2) "diam"(T)$. A good news is the probability $1-2exp(-u^2)$ make sense for $u >= sqrt(ln 2)$, then $4"diam"(T)$ can be absorb in $C u dot.c "diam"(T)$ with $u>= sqrt(ln 2)$ 
 by $C' = 4C$. 
  
  
]

#exercise(short:"Equivalence of Dudley's integral and sum")[
  Show the reverse bound:
  $
  integral_0^oo sqrt(log cal(N)(T,d,epsilon)) d epsilon <= C sum_(k in ZZ) 2^(-k) sqrt(log cal(N)(T,d,2^(-k)))
  $
]<Dudleys_equivalence>

#proof()[
  $
  2^(-k) sqrt(log cal(N)(T,d,2^(-k))) = integral_(2^(-k))^(2^(-k+1)) sqrt(log cal(N)(T,d,2^(-k))) d epsilon
  $
  
]

== Remarks and Examples

#remark(short: "Limits of Dudley's integral")[
  integral over $[0,"diam"(T)]$ is enough because for upper, the covering number is $1$ and its contribution is 0.
  
  *Question*: we can make it to countable infinity, but what about uncountable infinity set $T$ ?

  *Note*: the maximum of sub-gaussian only holds for $N>=2$. The reson why for $N=1$ it is still zero is because we can choose the same point as the covering set. 
]

#theorem(short: [Dudley's inequality for sets in $RR^n$])[
  For any set $T subset RR^n$, we have
  $
  w(T) <= C integral_0^oo sqrt(log cal(N)(T,epsilon)) d epsilon
  $
]

#proof()[
  $w(T) = sup_(t in T) angle.l t, g angle.r, g tilde.op N(0,1)$, $EE  angle.l t, g angle.r = 0$. 
  Then consider increments, by @example1, we have $norm(X_t-X_s)_(psi_2) <= C_2 norm(t-s)_2$. 
  By @Dudleys_inequality, we immediately have
  $
  w(T) <= C integral_0^oo sqrt(log cal(N)(T,epsilon)) d epsilon
  $
  
]

#example()[
  Let $T = B_2^n$, then 
  $
  w(B_2^n) <= C integral_0^("diam"(T)) sqrt(log cal(N)(B_2^n, d, epsilon)) d epsilon
  $
  and we can get 
  $
  cal(N)(B_2^n, d, epsilon) <= (3/epsilon)^n " for " epsilon in (0,1]. 
  $
  Here $d(dot.c, dot.c) = norm(dot.c)_1$. Then $cal(N)(B_2^n, d, epsilon)=1$ for $epsilon > 1$. We have 
  $
  w(B_2^n) <= C integral_0^1 sqrt(n log 3/epsilon) d epsilon <= C_1 sqrt(n).
  $
  
  $
  integral_0^1 sqrt(log 3/epsilon) d epsilon <= & sum_(k=1)^oo 1/2^k sqrt(log(3dot.c 2^k)) \
  <=& sum_(k=1)^oo 1/2^k sqrt(log 3 + k log 2) \
  <= & sum_(k=1) 1/2^k (log 3 + k log 2) \
  <= & log 3 + 3 log 2, "by" 1/(1-x)^2 = 1 + sum_k (k+1)x^k, " for " x in [0,1)
  $
]

#exercise(short:[Dudley's inequality can be loose])[
  Let $e_1, dots, e_n$ denote the canonical basis vectors in $RR^n$. Consider the set 
  $
  T := {e_k/sqrt(1+log k), k = 1, dots, n}.
  $

  #enum(
    numbering: "(a)",
    [
      Show that
      $
      w(T) <= C,
      $
      where as usual C denotes an absolute constant.
    ],
    [
      Show that
      $
      integral_0^oo sqrt(log cal(N)(T,d,epsilon) )d epsilon -> oo
      $
      as $n -> oo$.
    ],
)
]

#proof()[
 #enum(
   numbering: "(a)",
   [
     $ w(T) = max_(k in [1,n]) abs(g_k)/sqrt(1+log k) <= C_1 K $
     where $K = max norm(g_k)_(psi_2) <= C_2$. By @maximum_of_subgaussian
   ],
   [
     The first m vectors form a $mu$-separated set, where
     $
     mu^2 = max_(1 <= i < j <= m) 1/(1+log i) + 1/(1+log j) >= 2/(1+log m) >= 1/(log m) " for " m >= 3
     $
     Then we have $cal(N)(T,d,1/(log m)) >= m, " for " 3 <= m <= n$
     then
     $
     integral_0^oo sqrt(log cal(N)(T,d,epsilon)) d epsilon >= integral_(1 / (log n))^(1/(log m)) sqrt(m) d epsilon >= sqrt(m)/(2log m) "for" m = sqrt(n)
     $
     As $n->oo$, $root(4,n)/log n -> oo$
   ],
 )
]

== Two-sided Sudakov's inequality

skip

= Application: empirical processes

== Monte-Carlo method

Suppose we want to evaluate the integral of a function $f : Omega -> RR$ with respect to some probability measure $mu$ on some domain $Omega subset RR^d$: $ integral_Omega f d mu $

Which means that, there is a probability density on Domain $Omega$, that is $ PP[x in] = mu(A) "for all" A subset Omega $

We can use Monte-Carlo method to estimate $EE f(x)$ by sample $X_i$ with probability $mu(x)$. And by the law of large numbers, for i.i.d $X_i$ we have 
$
1/n sum_i f(X_i) -> EE f(x) "almost surely"
$

== A uniform law of large numbers

The problem is we cannot use the same samples $X_i$ to estimate every function. 
*Q:* why we need use the same samples? 

*Note*: one reason maybe is we cannot assume what function do we estimate now. So different function have different coefficient. Although the average error can be bounded by $O(1/sqrt(n))$, there might be a constant determinded by the function $f$. And there might be a set of function that form a infinity set, like function $f_i$ can be bounded by $i/sqrt(n)$, then function $f_n$ cannot be bounded.

But for class of Lipschitz function, we can get a good bound. Let 
$ cal(F) := {f : [0, 1] → RR, norm(k)_"Lip" <= L} $
where $L$ is an fixed number. 

#theorem(short:"Uniform law of large numbers")[
  Let $X,X_1,X_2,...,X_n$ be i.i.d. random variables taking values in $[0,1]$. Then
  $ EE sup_(f in cal(F)) abs(1/n sum_(i=1)^n f(X_i) - EE f(x)) <= (C L)/sqrt(n) $<8.20>
]<uniform_law_of_large_numbers>

To prepare for the proof of @uniform_law_of_large_numbers, it will be useful to view the left-hand side of @8.20 as the magnitude of a random prcess indexed by functions $f in cal(F)$. Such random processes are called _empirical processes_.

*Q:* what does magnitute mean here.

*A:* magnitute means $sup_t abs(X_t)$

#definition()[
  Let $cal(F)$ be a class of real-valued functions $f: Omega -> RR$, where $(Omega, Sigma, mu)$ is a probability space. Let $X$ be a random point in $Omega$ distributed according to the law $mu$, and let $X_1,X_2,...,X_n$ be independent copies of $X$. The random process $(X_f)_(f in cal(F))$ defined by $ X_f := 1/n sum_(i=1)^n f(X_i) - EE f(X) $
  is called an _empirical process_ indexed by $cal(F)$.
]

*Q*: what does $Sigma, mu$ mean here? If we denote $mu$ as probability density, why we need a $Sigma$ ?

#proof()[
  proof of @uniform_law_of_large_numbers.
  Without loss of generality, it is enough to prove the theorem for the class $ cal(F) := {f:[0,1] -> [0,1], norm(f)_"Lip" <= 1} $
  *Q:* why? How to generalize for every $Omega$? 

  *A:* Because in @uniform_law_of_large_numbers, it only says that $X_i$ is i.i.d. in $[0,1]$. By Lipschitz, it is bounded. So we can scale its value in $[0,1]$, and take the Lipschitz coefficent $L$ out. 

  We would like to bound the maginitude $ EE sup_(f in cal(F)) abs(X_f) $

  step 1: Check for sub-gaussian increments and the using @Dudleys_inequality.
  $ norm(X_f-X_g)_(psi_2) = 1/n norm(sum_(i=1)^n f(X_i) - EE f(X_i) - g(X_i) + EE g(X_i))_(psi_2) $, denote $Z_i = (f-g)(X_i) - EE(f-h)(X_i)$. 

  Note That $EE Z_i = 0$, we have $ norm(sum Z_i)_(psi_2)^2 lt.tilde sum norm(Z_i)_(psi_2)^2 $
  Then by centering Lemma, we have 
  $
  norm(Z_i)_(psi_2) lt.tilde norm((f-g)(X_i))_(psi_2) lt.tilde norm(f-g)_oo
  $ 
  The last $lt.tilde$ from the probability of sub-gaussian of bounded random variable.

  So $ norm(X_f-X_g)_(psi_2) lt.tilde 1/sqrt(n)norm(f-g)_oo $

  step 2: By @Dudleys_inequality, and with $f_0: [0,1] -> 0$, we have
  $ EE sup_(f in cal(F))abs(X_f) = EE sup_(f in cal(F)) abs(X_f - X_0) lt.tilde 1/sqrt(n) integral_0^1 sqrt(log cal(N)(cal(F), norm(dot.c)_oo, epsilon)) d epsilon $
  The upper side of integral is because $"diam"(cal(F))$ in $L^oo$ metric is bounded by 1 by definition of $cal(F)$. That is, for every $f,g in cal(F)$, and $x in[0,1]$, $abs(f(x)-g(x)) <= 1$, so $norm(f-g)_oo <= 1$.

  *Q*: Does $norm(f-g)_oo$ mean $sup_x abs((f-g)(x))$?

  If we can bound $ cal(N)(cal(F), norm(dot.c)_oo, epsilon) <= (C/epsilon)^(C "/" epsilon) $, then
  $
  EE sup_(f in cal(F)) lt.tilde 1/sqrt(n) integral_0^1 sqrt(C/epsilon log C/epsilon) d epsilon lt.tilde 1/sqrt(n)
  $
]

#exercise(short:[Metric entropy of the class of Lipschitz functions])[
  Consider the class of functions 
  $
  cal(F) := {f:[0,1] -> [0,1], norm(f)_"Lip" <= 1}
  $
  Show that $ cal(N)(cal(F),norm(dot.c)_oo, epsilon) <= (2/epsilon)^(2 "/" epsilon) " for any " epsilon > 0 $
]<Metric_entropy_of_Lip>
#proof()[
  
]

#exercise(short:"An improved bound on the metric entropy")[
  Improve the bound in @Metric_entropy_of_Lip to 
  $ 
  cal(N)(cal(F),norm(dot.c)_oo, epsilon) <= e^(C "/" epsilon) " for any " epsilon > 0 
  $
]

#proof()[
  
]

#exercise(short:[Higher dimensions])[
  Consider the class of functions
  $ cal(F) := {f:[0,1]^d -> RR, f(0)=0, norm(f)_"Lip" <= 1} $
  for some dimension $d >= 1$. Show that 
  $ 
  cal(N)(cal(F),norm(dot.c)_oo, epsilon) <= e^(C "/" epsilon^d) " for any " epsilon > 0 
  $
]
== Empirical measure
Consider a probability measure $mu_n$ that is uniformly distributed on the sample $X_1,...,X_N$, that is, 
$
mu_n({X_i}) = 1/n " for every " i = 1,...,n 
$
is called the _empirical measure_.

Then @uniform_law_of_large_numbers can be seen as a bound of original process and empirical process. 

= VC dimension

next Note. 

#bibliography("works.bib", full: true, style: "institute-of-electrical-and-electronics-engineers")